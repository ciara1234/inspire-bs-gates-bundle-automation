function () {

  var env = karate.env;
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'test';
  }

  var props = read('classpath:karate-properties.yml')
  var baseUrl = com.iii.inspire.util.GetEnvVariable.getDockerHost();

  var config = {
    env: env,
    // load property values from karate-properties.yml
    gates_api_url: baseUrl + props.gatewayPort + props.gatesApiContext,
    kafka_rest_url: baseUrl + props.kafkaRestPort,
    sapi_url: props.sapiUrl
  }

  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 50000);
  karate.configure('logPrettyRequest', true);
  karate.configure('logPrettyResponse', true);
  return config;
}
