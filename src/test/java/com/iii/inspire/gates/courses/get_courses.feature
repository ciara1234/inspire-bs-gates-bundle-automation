@resource_courses
Feature: Get course reserve information

  Background:
    * url gates_api_url

  Scenario: 01 Get courses data
    * configure headers = read('classpath:sierra-headers.js')
    Given path '/courses'
    And param limit = '20'
    And param offset = '0'
    When method get
    Then status 200

