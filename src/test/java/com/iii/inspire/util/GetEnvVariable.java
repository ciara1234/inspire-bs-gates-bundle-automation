package com.iii.inspire.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetEnvVariable {

  private static final Logger LOG = LoggerFactory.getLogger(GetEnvVariable.class);

  public static String getDockerHost() {
    String host;
    if(isNotJenkins()) {
      host = System.getenv("DOCKER_HOST");
    } else {
      host = "0.0.0.0:2375";
    }
    if (host == null) {
      LOG.error("DOCKER_HOST must be set.");
      return "DOCKER_HOST not set";
    }
    Matcher matcher = Pattern.compile("([0-9].*[0-9]:).+").matcher(host);
    matcher.find();
    String ipOnly = matcher.group(1);
    return "http://" + ipOnly;
  }

  public static Properties getKarateProps() {
    Properties props = new Properties();
    try {
      String path = GetEnvVariable.class.getClassLoader()
          .getResource("karate-properties.yml").toURI().getPath();
      props.load(new FileInputStream(path));
    } catch (URISyntaxException | IOException e) {
      LOG.error("Failed to load karate-properties.yml", e);
    }
    return props;
  }

  public static boolean isNotJenkins() {
    return System.getenv("JENKINS_HOME") == null;
  }
}
