@getGatesKey
@ignore
Feature: Get Gates Key on Sierra API
  Background:
    * def wait = read('classpath:wait-util.js')

  Scenario: Connect to SAPI as iii and get SAPI Token
    Given url sapi_url +'/v5/'+ 'token'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Basic aWlpOjI5NjYzZGV2'
    And header grant_type = 'client_credentials'
    And request {}
    When method post
    Then status 200

    * call wait 2
    * header Authorization = 'Bearer ' + response.access_token

    Given url sapi_url +'/v5/'+ 'apiKeys?role=GATES'
    And header Content-Type = 'application/json'
    When method get
    Then status 200


