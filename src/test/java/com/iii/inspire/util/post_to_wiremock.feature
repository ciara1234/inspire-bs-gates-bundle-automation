@ignore
Feature: Create stub by request

  Scenario: Create stub in Wiremock - request body passed as argument from calling feature

    * def resJson = __arg
    * print resJson

    Given url 'http://192.168.99.100:8080/__admin/mappings'
    And request resJson
    When method post
    Then status 201
