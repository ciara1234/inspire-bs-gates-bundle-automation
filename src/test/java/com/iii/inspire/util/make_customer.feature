@customer
@ignore
Feature: Create Customer Stub in Stubbed Customer API

  Background:
    * def resJson = __arg
    * print resJson
    * def customerJson = read('../../util/customer.json')
    * def doStuff =
    """
     function(json,key,secret)
     {
        json.response.jsonBody[0].clientId = key
        json.response.jsonBody[0].clientSecret = secret
        karate.call('../../util/post_to_wiremock.feature',json );
    }
    """

    Scenario: Add fresh SAPI key to wiremock stub and post it in.
    * eval doStuff(customerJson, resJson.key,'goodsecret')
