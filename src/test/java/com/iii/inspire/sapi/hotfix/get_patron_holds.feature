@canfreezeallholds
Feature: Check patrons holds endpoint for new field canFreeze
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2
    * def patron_id = 2183478
  # 2183607 Tom Cruise

  Scenario: Clear any holds for patron

    Scenario: Get all holds for patron
    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds?fields=default'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200

  Scenario: Clear any holds for patron
      Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
      And header Content-Type = 'application/json'
      * header Authorization = 'Bearer ' + sapi.response.access_token
      When method delete
      * def uploadStatusCode = responseStatus
      Then assert responseStatus == 204 || responseStatus == 404

  Scenario: Place synchronous hold for the patron
    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1085508,"pickupLocation": "a","pickupType": "holdshelf"}
    When method post
    Then status 200


