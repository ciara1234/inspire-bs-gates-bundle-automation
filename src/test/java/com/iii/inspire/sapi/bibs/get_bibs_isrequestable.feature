@sapibibs
Feature: Check bibs endpoint for new field isRequestable
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2

  Scenario: Try to get a bib that is not deleted and not suppressed with field isRequestable and it returns the field

    Given url sapi_url + '/v5/'+ 'bibs/?limit=1&suppressed=false&deleted=false&fields=isRequestable'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.entries[0].isRequestable == true || responseBody.entries[0].isRequestable == false

  Scenario: Try to get multiple bibs by id that are not deleted and not suppressed with field isRequestable and it returns the field

    Given url sapi_url + '/v5/'+ 'bibs/?id=1000001,1000002,1000003&suppressed=false&deleted=false&fields=isRequestable'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.entries[0].isRequestable == true || responseBody.entries[0].isRequestable == false
    And assert responseBody.entries[1].isRequestable == true || responseBody.entries[1].isRequestable == false
    And assert responseBody.entries[2].isRequestable == true || responseBody.entries[2].isRequestable == false



  Scenario: Try to get multiple bibs that are not deleted and not suppressed with field isRequestable and it returns the field

    Given url sapi_url + '/v5/'+ 'bibs/?limit=3&suppressed=false&deleted=false&fields=isRequestable'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.entries[0].isRequestable == true || responseBody.entries[0].isRequestable == false
    And assert responseBody.entries[1].isRequestable == true || responseBody.entries[1].isRequestable == false
    And assert responseBody.entries[2].isRequestable == true || responseBody.entries[2].isRequestable == false

  Scenario: Try to get a deleted bib with field isRequestable it gives 500 Xcirc Error

    Given url sapi_url + '/v5/'+ 'bibs/?limit=1&deleted=true'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200

    * def bib_id = response.entries[0].id

    Given url sapi_url + '/v5/'+ 'bibs/?limit=1&fields=isRequestable&id=' + bib_id
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 500
    And match response.description == "XCirc error : Can't load record info"

  Scenario: Try to get a suppressed bib with field isRequestable and it returns the field

    Given url sapi_url + '/v5/'+ 'bibs/?limit=1&suppressed=true'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def bib_id = response.entries[0].id

    Given url sapi_url + '/v5/'+ 'bibs/?limit=1&fields=isRequestable&id=' + bib_id
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.entries[0].isRequestable == true || responseBody.entries[0].isRequestable == false

  Scenario: Try to get a random bib with field isRequestable with invalid input for bib id

    Given url sapi_url + '/v5/'+ 'bibs/?id=sfsf&fields=isRequestable'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 500
    And match response.description == "Number format error"





