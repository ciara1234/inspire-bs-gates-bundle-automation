@sapicourse
Feature: Check new courses endpoint
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2

  Scenario: Try to get courses with limit of 50 and offset of 0

    Given url sapi_url + '/v5/'+ 'courses?limit=50&offset=0'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 50
    And assert responseBody.start == 0


  Scenario: Try to get courses with limit of 1 and offset of 1

    Given url sapi_url + '/v5/'+ 'courses?limit=1&offset=1'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 1
    And assert responseBody.start == 1


  Scenario: Try to get courses with default fields

    Given url sapi_url + '/v5/'+ 'courses?fields=default'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200



  Scenario: Try to get courses with certain fields

    Given url sapi_url + '/v5/'+ 'courses?fields=default,ccode1,ccode2,ccode3'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200

  Scenario: Try to get courses with bad fields

    Given url sapi_url + '/v5/'+ 'courses?fields=holdId'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 400
    And match response.name == "Invalid parameter"
    And match response.description == "Invalid parameter : holdId is not a valid field for class com.iii.api.courses.model.Course"



  Scenario: Try to get courses by a single course id and validate output structure

    Given url sapi_url + '/v5/'+ 'courses?limit=1&offset=0'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200

    * def course_id = response.entries[0].id

    Given url sapi_url + '/v5/'+ 'courses?id='+ course_id
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def expectedResponse =
     """
      {
        "total": 1,
        "entries": [
          {
            "id": "##string",
            "beginDate": "##string",
            "endDate": "##string",
            "locationCode": "##string",
            "ccode1": "##string",
            "ccode2": "##string",
            "ccode3": "##string",
            "professorInstructor": "##string",
            "courseName": "##string",
            "courseUrl": "##string",
            "courseNote": "##string",
            "reserves": [
              {
                "id": "##string",
                "recordType": "##string",
                "until": "##string",
                "statusCode": "##string"
              },
                {
                "id": "##string",
                "recordType": "##string",
                "until": "##string",
                "statusCode": "##string"
              },
                {
                "id": "##string",
                "recordType": "##string",
                "until": "##string",
                "statusCode": "##string"
              }
            ]
          }
        ]
      }
     """
    * match response contains expectedResponse

