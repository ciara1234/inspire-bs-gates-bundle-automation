@sapicourse
Feature: Check new courses endpoint - failure scenario
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2


  Scenario: Try to get courses with invalid id input (text)

    Given url sapi_url + '/v5/'+ 'courses?limit=1&offset=0&id=adadad'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 500
    * def expectedResponse =
    """
    {
      "code": 102,
      "specificCode": 0,
      "httpStatus": 500,
      "name": "Internal server error",
      "description": "Number format error"
    }
    """
    * match response contains expectedResponse


