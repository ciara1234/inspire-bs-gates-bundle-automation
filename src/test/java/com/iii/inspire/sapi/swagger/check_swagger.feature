Feature: Check Swagger Documentation for Courses Endpoint
  Background:
    * def wait = read('classpath:wait-util.js')
    * call read('common.feature')
    * configure driver = { type: 'chrome' }


  Scenario: try to login swagger and check api docs are OK

    * driver 'https://devops-8963-app.iii-lab.eu/iii/sierra-api/swagger/index.html'
    * call wait 5
    * driver.input('#input_clientKey', '9TyxorUpRMzfC+loHLOz5h9LuBHF')
    * driver.input('#input_clientSecret', 'goodsecret')
    #* s.type(Key.ENTER)
    * call wait 2
    * driver.click('^Get_a_list_of_course_reserves_get_0')
    * call wait 90
    * def bytes = driver.screenshot()
    * eval karate.embed(bytes, 'image/png')
