@synchhold
Feature: Check patrons holds endpoint for placing synchronous holds - GET IT
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2
    * def patron_id = 2183478
  # 2183607 Tom Cruise

  Scenario: Place synchronous hold for the patron for holdshelf

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf"}
    When method post
    Then status 200
    * def hold_link = response.link

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds?limit=1&fields=default'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    And match response.entries[0].id == '#(hold_link)'


  Scenario: Place synchronous hold for the patron but they already got it on hold

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf"}
    When method post
    Then status 200

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf"}
    When method post
    Then status 500
    And match response.name == "XCirc error"
    And match response.description == "XCirc error : Request denied - already on hold for or checked out to you."


  Scenario: Place synchronous hold for the patron for holdshelf and needed by date in YYYY-MM-DD format

    * def neededBy = "2020-01-01"

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf", "neededBy": #(neededBy) }
    When method post
    Then status 200
    * def hold_link = response.link

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds?limit=1&fields=default'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    And match response.entries[0].notNeededAfterDate == '#(neededBy)'
    And match response.entries[0].id == '#(hold_link)'


  Scenario: Place synchronous hold for the patron for holdshelf and needed by date is bad

    * def neededBy = "2020-30-01"

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf", "neededBy": #(neededBy) }
    When method post
    Then status 400
    And match response.name == "Invalid JSON"
    And match response.description == "JSON object missing field or field has invalid data"

  Scenario: Place synchronous hold for the patron for holdshelf and note

    * def note = "yeet!"

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf", "note": #(note)}
    When method post
    Then status 200
    * def hold_link = response.link

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds?limit=1&fields=default,note'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    And match response.entries[0].note == '#(note)'
    And match response.entries[0].id == '#(hold_link)'

  Scenario: Place synchronous hold for the patron for shipshelf

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "shipshelf"}
    When method post
    Then status 200
    * def hold_link = response.link

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds?fields=default'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    And match response.entries[0].id == '#(hold_link)'


  Scenario: Place synchronous hold for the patron for bad pickuptype

    * def pickuptype = "elfonashelf"

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": '#(pickuptype)'}
    When method post
    Then status 400
    And match response.name == "Invalid JSON"
    And match response.description == "Invalid JSON : Required element 'pickupType' must be either 'shipshelf' or 'holdshelf'"


  Scenario: Place synchronous hold for the patron for holdshelf and item

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "i","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf"}
    When method post
    Then status 500
    And match response.name == "Record type is not supported"
    And match response.description == "Record type is not supported : Bib records are only supported"


  Scenario: Place synchronous hold for the patron for holdshelf and volume

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "j","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf"}
    When method post
    Then status 500
    And match response.name == "Record type is not supported"
    And match response.description == "Record type is not supported : Bib records are only supported"


  Scenario: Place synchronous hold for the patron for holdshelf and bad bib
    * def bad_bib_id = 218
    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 204 || responseStatus == 404

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": #(bad_bib_id),"pickupLocation": "bd","pickupType": "holdshelf"}
    When method post
    Then status 500
    And match response.name == "XCirc error"
    And match response.description == "XCirc error : An internal error has occurred. Please contact the library staff for further assistance."

  Scenario: Place synchronous hold for the patron who doesn't exist

    * def bad_patron_id = 218

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + bad_patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204954,"pickupLocation": "bd","pickupType": "holdshelf"}
    When method post
    Then status 500
    And match response.name == "XCirc error"
    And match response.description == "XCirc error : Sorry, cannot load your patron record"

