@isrenewable
Feature: Check patrons checkouts by patron id endpoint for new field isRenewable
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2
    * def patron_id = 2183607
  # 2183607 Tom Cruise


  Scenario: Get all checkouts for patron with isRenewable field

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/checkouts?fields=isRenewable'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.entries[0].isRenewable == true || responseBody.entries[0].isRenewable == false


  Scenario: Get all checkouts for patron with default and isRenewable fields

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/checkouts?fields=default,isRenewable'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.entries[0].isRenewable == true || responseBody.entries[0].isRenewable == false
