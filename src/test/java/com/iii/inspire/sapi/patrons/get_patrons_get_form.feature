@getform
Feature: Check patrons get form
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2
    * def patron_id = 2183607

  # 2183607 Tom Cruise

  Scenario: Call getform for a valid patron and bib type and bib record id
    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds/requests/getform?recordType=b&recordNumber=1204955'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.notNeededAfter == true || responseBody.notNeededAfter == false
    And assert responseBody.holdshelf.locations[0].code == "ap   "
    And assert responseBody.holdshelf.locations[0].name == "Algona-Pacific"
    And assert responseBody.shipshelf.locations[0].code == "home "
    And assert responseBody.shipshelf.locations[0].name == "Patron's Home for $3.50"

  Scenario: Get form with a valid patron and bib type and invalid bib record id - out of range
    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds/requests/getform?recordType=b&recordNumber=222222222222222222222222222222222222222'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 400
    And match response.name == "Invalid argument"
    And match response.description == "Invalid argument : Bibliographic record number can't be converted to number"

  Scenario: Call getform for a valid patron and item record type and item record id
    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds/requests/getform?recordType=i&recordNumber=1000002'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 500
    * def responseBody = response
    And match response.name == "Record type is not supported"
    And match response.description == "Record type is not supported : Bib records are only supported"

  Scenario: Get form with a invalid patron and bib type and bib record id
    Given url sapi_url + '/v5/'+ 'internal/patrons/' + '12345' + '/holds/requests/getform?recordType=b&recordNumber=1204955'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 404
    And match response.name == "Record not found"
    And match response.description == "Record not found : Patron record cannot be loaded."

  Scenario: Get form with a deleted patron and bib type and bib record id
    Given url sapi_url + '/v5/'+ 'internal/patrons/' + '1505485' + '/holds/requests/getform?recordType=b&recordNumber=1204955'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 404
    And match response.name == "Record not found"
    And match response.description == "Record not found : Patron record not found."

  Scenario: Get form with a blocked patron and bib type and bib record id
    Given url sapi_url + '/v5/'+ 'internal/patrons/' + '2183484' + '/holds/requests/getform?recordType=b&recordNumber=1204955'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 400
    And match response.name == "Request Validation failed"
    And match response.description == "Request Validation failed : Patron is blocked."

    Scenario: Get form with a valid patron and a deleted bib id

      Given url sapi_url + '/v5/'+ 'bibs/?limit=1&deleted=true'
      And header Content-Type = 'application/json'
      * header Authorization = 'Bearer ' + sapi.response.access_token
      When method get
      Then status 200

      * def bib_id = response.entries[0].id

      Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds/requests/getform?recordType=b&recordNumber=' + bib_id
      And header Content-Type = 'application/json'
      * header Authorization = 'Bearer ' + sapi.response.access_token
      When method get
      Then status 404
      And match response.name == "Record not found"
      And match response.description == "Record not found : Bibliographic record not found."


  Scenario: Get form with a valid patron and suppressed bib id

    Given url sapi_url + '/v5/'+ 'bibs/?limit=1&suppressed=true'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def bib_id = response.entries[0].id

    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds/requests/getform?recordType=b&recordNumber=' + bib_id
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 400
    And match response.name == "Request Validation failed"
    And match response.description == "Request Validation failed : Bibliographic record is suppressed."



