@canfreezeallholds
Feature: Check patrons all holds endpoint for new field canFreeze
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2
    * def patron_id = 2183607
  # 2183607 Tom Cruise

  Scenario: Clear any holds for patron

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method delete
    * def uploadStatusCode = responseStatus
     Then assert responseStatus == 204 || responseStatus == 404

  Scenario: Place synchronous hold for the patron
    Given url sapi_url + '/v5/'+ 'internal/patrons/' + patron_id + '/holds'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request { "recordType": "b","recordNumber": 1204955,"pickupLocation": "nb","pickupType": "holdshelf"}
    When method post
    Then status 200

  Scenario: Get all holds for patron, checking for canFreeze and validate response structure
    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds?fields=canFreeze'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def expectedResponse =
  """
  {
  "total": #number,
  "entries": [
  {
  "id": #string,
  "canFreeze": #boolean
  }
  ]
  }
  """
    * match response contains expectedResponse


  Scenario: Get all holds for patron requesting default fields and canFreeze field
    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds?fields=default,canFreeze'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.entries[0].canFreeze == true || responseBody.entries[0].canFreeze == false

  Scenario: Get all holds for patron requesting the canFreeze field
    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/holds?fields=canFreeze'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.entries[0].canFreeze == true || responseBody.entries[0].canFreeze == false



