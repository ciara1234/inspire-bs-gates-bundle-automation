@isrenewable
Feature: Check patrons checkouts by patron id endpoint for new field isRenewable
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2
    * def patron_id = 2183607
  # 2183607 Tom Cruise


  Scenario: Get all checkouts for patron and check for isRenewable field on an individual checkout

    Given url sapi_url + '/v5/'+ 'patrons/' + patron_id + '/checkouts'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200

   # * def checkout_id1 = response.entries[0].id
   # * def checkout_id2 = response.entries[0].id

  * def checkout_id1 = 1914
  * def checkout_id2 = 1917

    Given url sapi_url + '/v5/'+ 'patrons/checkouts/' + checkout_id1 + '?fields=isRenewable'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.isRenewable == true || responseBody.isRenewable == false

    Given url sapi_url + '/v5/'+ 'patrons/checkouts/' + checkout_id2 + '?fields=default,isRenewable'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.isRenewable == true || responseBody.isRenewable == false
