@canfreezeonehold
Feature: Check patrons holds by hold id endpoint for new field canFreeze
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../../util/get_sapi_token.feature')
    * call wait 2
    * def patron_id = 2183478

  Scenario: Try to get a bib level hold with field canFreeze and it passes

    Given url sapi_url + '/v5/'+ 'patrons/holds/4433?fields=canFreeze'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def expectedResponse =
    """
   {
    "id": "#string",
    "canFreeze": #boolean
   }
    """
    * match response contains expectedResponse


  Scenario: Try to get a item level hold with field canFreeze and it passes

    Given url sapi_url + '/v5/'+ 'patrons/holds/3952?fields=canFreeze'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
    * def expectedResponse =
    """
   {
    "id": "#string",
    "canFreeze": #boolean
   }
    """
    * match response contains expectedResponse

  Scenario: Try to get a non existing hold with field canFreeze and it fails

    Given url sapi_url + '/v5/'+ 'patrons/holds/1?fields=canFreeze'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 404
    * def expectedResponse =
    """
    {
      "code": 107,
      "specificCode": 0,
      "httpStatus": 404,
      "name": "Record not found"
    }
    """
    * match response contains expectedResponse


  Scenario: Try to get a hold with field canFreeze with invalid input and it fails

    Given url sapi_url + '/v5/'+ 'patrons/holds/aaa?fields=canFreeze'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 500
    * def expectedResponse =
    """
  {
  "code": 102,
  "specificCode": 0,
  "httpStatus": 500,
  "name": "Internal server error",
  "description": "Number format error"
  }
    """
    * match response contains expectedResponse

