package com.iii.inspire;

import static org.junit.Assert.assertTrue;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import cucumber.api.CucumberOptions;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import com.intuit.karate.KarateOptions;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Runs all tests but expects docker-compose to be up and running by docker-compose gradle plugin.
 */
@CucumberOptions(tags = {"@gateskey","@sapicourse","@sapibibs","@canfreezeonehold","@canfreezeallholds","@resource_courses"})
public class AllTestsJenkins {

  @Test
  public void testAll() throws InterruptedException {

    Thread.sleep(1000 * 60 * 1); // NOSONAR
    System.setProperty("karate.env", "test");
    Results results = Runner.parallel(getClass(), 5);
    generateReport(results.getReportDir());
    assertTrue(results.getErrorMessages(), results.getFailCount() == 0);

  }
    public static void generateReport(String karateOutputPath) {
      Collection<File> jsonFiles = FileUtils
          .listFiles(new File(karateOutputPath), new String[] {"json"}, true);
      List<String> jsonPaths = new ArrayList(jsonFiles.size());
      jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
      Configuration config = new Configuration(new File("target"), "test");
      ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
      reportBuilder.generateReports();
    }


    /**   String karateOutputPath = Paths.get("build", "surefire-reports").toString();
       System.out.println("karateOutputPath" + karateOutputPath);
       KarateStats stats = CucumberRunner.parallel(getClass(), 1, karateOutputPath);
       stats.printStats(1);
       String msg = String.format("There are %d scenario failures.\nFull report: %s",
           stats.getFailCount(),
           "conv-bs-gates-bundle-automation/build/cucumber-html-reports/overview-features.html");

       assertTrue(msg, stats.getFailCount() == 0);
     */

}
