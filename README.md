# inspire-bs-gates-bundle-automation

Steps:

Work in progress to automate bundle testing of Sierra and SAPI for Inspire Gates

Config your devops sierra server in karate-properties.yml
Bring up docker

For Gates
Run create_gates_api_key.feature to create a gates api key
  - you can then run gates\courses\get_courses.feature

For SAPI

Anything in sapi folder is calling SAPI directly
  - bibs
  - synchronous holds
  - courses
  - experimental checking of swagger using karate ui automation driver
